

resource "gitlab_project" "php-app" {
  name = "php-app"
  default_branch = "master"
}

resource "gitlab_project" "python-app" {
  name = "python-app"
  default_branch = "master"
}

resource "gitlab_project" "main-site" {
  name = "waterslurry.gitlab.io"
  visibility_level = "public"
  default_branch = "master"
  request_access_enabled = false
}

resource "gitlab_project" "go-app" {
  provider = gitlab
  name = "go-app"
  default_branch = "master"
}

resource "gitlab_project_variable" "go-app-heroku" {
  key = "HEROKU_APP_NAME"
  project = gitlab_project.go-app.id
  value = heroku_app.goapp.name
}
