variable "gitlab_token" {
  default = ""
}

variable "cloudflare_email" {
  default = ""
}

variable "cloudflare_api_key" {
  default = ""
}

variable "heroku_email" {
  default = ""
}
variable "heroku_api_key" {
  default = ""
}

variable "cloudflare_zone_id" {
  default = ""
}

variable "domain" {
  default = ""
}

variable "mailgun_domain" {
  default = ""
}

variable "mailgun_apikey" {
  default = ""
}

variable "mailgun_target_email" {
  default = ""
}