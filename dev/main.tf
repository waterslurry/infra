
provider "gitlab" {
  token = var.gitlab_token
}

provider "cloudflare" {
  version = "~> 2.0"
  email   = var.cloudflare_email
  api_key = var.cloudflare_api_key
}

provider "heroku" {
  email   = var.heroku_email
  api_key = var.heroku_api_key
}



