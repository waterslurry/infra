terraform {
  required_providers {
    cloudflare = {
      source = "terraform-providers/cloudflare"
    }
    gitlab = {
      source = "terraform-providers/gitlab"
    }
    heroku = {
      source = "terraform-providers/heroku"
    }
  }
  required_version = ">= 0.13"
}
