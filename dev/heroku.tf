
resource "heroku_app" "goapp" {
  name   = "water-slurry-go-app"
  region = "us"

  config_vars = {
    MAILGUN_API_KEY = var.mailgun_apikey
    MAILGUN_TARGET_EMAIL = var.mailgun_target_email
    MAILGUN_DOMAIN = var.mailgun_domain
  }

  buildpacks = [
    "heroku/go"
  ]
}

resource "heroku_app" "phpapp" {
  name   = "water-slurry-php-app"
  region = "us"

  config_vars = {
    MAILGUN_API_KEY = var.mailgun_apikey
    MAILGUN_TARGET_EMAIL = var.mailgun_target_email
    MAILGUN_DOMAIN = var.mailgun_domain
  }

  buildpacks = [
    "heroku/php"
  ]
}

resource "heroku_app" "pythonapp" {
  name   = "water-slurry-python-app"
  region = "us"

  config_vars = {
    MAILGUN_API_KEY = var.mailgun_apikey
    MAILGUN_TARGET_EMAIL = var.mailgun_target_email
    MAILGUN_DOMAIN = var.mailgun_domain
  }

  buildpacks = [
    "heroku/python"
  ]
}

#for paid services only
#resource "heroku_domain" "goapp" {
#app      = heroku_app.goapp.name
#hostname = "${cloudflare_record.goapp.name}.${var.domain}"
#}